//
//  ViewController.swift
//  iOS_Task
//
//  Created by kunchal on 29/10/21.
//

import UIKit

let CELL = "ListCell"
let HeaderCELL = "HeaderCell"
let ImageCELL = "ImageCell"


class ViewController: UIViewController,UIScrollViewDelegate {

    
    @IBOutlet weak var homeTableView: UITableView!
    
    var currentpage = Int()

    var ListArray = [Any]()
   var ImagesArray = [ "1.jpg","2.jpg","3.jpg","4.jpg","5.jpg"]
    var filterArray = [Any]()
    
    var isSearch = Bool()
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        currentpage = 0
        self.homeTableView.register(UINib(nibName: CELL, bundle: nil), forCellReuseIdentifier: CELL)
        self.homeTableView.register(UINib(nibName: HeaderCELL, bundle: nil), forCellReuseIdentifier: HeaderCELL)
        self.homeTableView.register(UINib(nibName: ImageCELL, bundle: nil), forCellReuseIdentifier: ImageCELL)
        loadData()
    }
   
    
    var lastContentOffset: CGFloat = 0
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if self.lastContentOffset < scrollView.contentOffset.y {
            // did move up
        } else if self.lastContentOffset > scrollView.contentOffset.y {
            // did move down
        } else {
            // didn't move
            let x = scrollView.contentOffset.x
            let w = scrollView.bounds.size.width
            currentpage  = Int(ceil(x/w))
           filterArray =  ListArray[currentpage] as! [Any]
           homeTableView.reloadData()
        }
    }
    func loadData(){
        
        for i in 0..<5 {
            var listArray = [Any]()
            for j in 0..<100 {
              let str = "\(i)\(j)"
                listArray.append(str)
            }
            ListArray.append(listArray)
        }
        
        filterArray = ListArray[currentpage] as! [Any]
        self.homeTableView.delegate = self
        self.homeTableView.dataSource = self
        self.homeTableView.reloadData()
        
    }
}



extension ViewController:UITableViewDelegate,UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }else{
            return filterArray.count
        }
    }
    
  func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 1{
        return 60
        }else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 1 {
        let header = tableView.dequeueReusableCell(withIdentifier: HeaderCELL) as! HeaderCell
            header.homeSearchBar.delegate = self
            if isSearch {
                header.homeSearchBar.becomeFirstResponder()
            }else{
                header.homeSearchBar.resignFirstResponder()
            }
        return header
        }else{
            return UIView()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 226
        }else{
        return 80
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell =  tableView.dequeueReusableCell(withIdentifier: ImageCELL, for: indexPath) as! ImageCell
            cell.imageScrollview.delegate = self
            cell.pageController.currentPage = currentpage
            cell.imageArray = ImagesArray
            
            
            return cell
            
        }else{
        
        let cell =  tableView.dequeueReusableCell(withIdentifier: CELL, for: indexPath) as! ListCell
            let currentImageStr = ImagesArray[currentpage]
         //   let listArray = ListArray[currentpage] as! [Any]
        cell.cellimage?.image = UIImage(named: currentImageStr)
        cell.nameLBL?.text = filterArray[indexPath.row] as! String
        return cell
      }
 }

}

extension ViewController: UISearchBarDelegate{
    //MARK: UISearchbar delegate
        func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        DispatchQueue.main.async {
            let indexPath = IndexPath(row: NSNotFound, section: 1)
            self.homeTableView.scrollToRow(at: indexPath, at: .top , animated: true)
        }
    }
       
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        //   searchBar.resignFirstResponder()
        //   isSearch = false
    }
       
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
           searchBar.resignFirstResponder()
        filterArray =  ListArray[currentpage] as! [Any]
        homeTableView.reloadData()
        searchBar.text = ""
    }
       
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
           searchBar.resignFirstResponder()
           isSearch = false
        self.homeTableView.reloadData()

    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.count == 0 {
            isSearch = false
            self.homeTableView.reloadData()
        } else {
            let array = filterArray
            filterArray = array.filter({ (text) -> Bool in
                let tmp: NSString = text as! NSString
                let range = tmp.range(of: searchText, options: NSString.CompareOptions.caseInsensitive)
                return range.location != NSNotFound
            })
            if(filterArray.count == 0){
                isSearch = false
            } else {
                isSearch = true
            }
            self.homeTableView.reloadData()
            searchBar.endEditing(false)
        }
    }
    
    

}
