//
//  ImageCell.swift
//  iOS_Task
//
//  Created by kunchal on 29/10/21.
//

import UIKit

class ImageCell: UITableViewCell , UIScrollViewDelegate {

    
    @IBOutlet weak var backView: UIView!
    
    @IBOutlet var imageScrollview: UIScrollView!
    
    
    @IBOutlet var pageController: UIPageControl!
    var imageArray = [ "1.jpg","2.jpg","3.jpg","4.jpg","5.jpg"]
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        

        imageScrollview.delegate = self
        
        pageController.numberOfPages = imageArray.count
       // pageController.currentPage = 0
        
        backView.bringSubviewToFront(pageController)

        imageScrollview = UIScrollView(frame: CGRect(x: 0, y: 0, width: backView.bounds.width, height: backView.bounds.height - 30))
        imageScrollview?.showsHorizontalScrollIndicator = false
        imageScrollview?.isPagingEnabled = true
        for i in 0..<imageArray.count {
            let offset = i == 0 ? 0 : (CGFloat(i) * backView.bounds.width+10)
            let imgView = UIImageView(frame: CGRect(x: offset, y: 0, width: backView.bounds.width-10, height: imageScrollview.bounds.height))
            imgView.clipsToBounds = true
            imgView.image = UIImage(named: imageArray[i])
            imgView.contentMode = .scaleAspectFill
            imageScrollview!.addSubview(imgView)
        }
        imageScrollview!.contentSize = CGSize(width: CGFloat(imageArray.count) * self.backView.bounds.width, height: backView.bounds.height-50)
        self.contentView.addSubview(imageScrollview!)
    
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    
    
}

