//
//  HeaderCell.swift
//  iOS_Task
//
//  Created by kunchal on 29/10/21.
//

import UIKit

class HeaderCell: UITableViewCell {

    @IBOutlet var homeSearchBar: UISearchBar!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
